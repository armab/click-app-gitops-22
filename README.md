# Click App GitOps
Repository to build a Click flask python3 application that uses Redis as a backend.
Templated with Helm, and deployed to the EKS K8s cluster managed by the Terraform.
CI/CD with Gitlab-CI creates an ephemeral environment in the K8s cluster with a preview on every PR.

## Requirements
- Docker
- Kubernetes
- Helm
- Terraform

## Overview
### app
- Docker: package image to run process in the container as unprivileged `nobody` user

### Kubernetes & Helm
- K8s objects are templated with Helm
  - allows full deployments/rollbacks with history
  - configuration and parametrization with [values.yaml](./kubernetes/values.yaml)
- Components
  - Deployment for the Demo app
    - 2 replicas
  - LoadBalancer service for Demo app creates ALB in AWS
    - TODO: proper Ingress controller with TLS
  - StatefulSet & Headless Service for the Redis
    - 1 replica only, no HA, self-coded
    - TODO prod: external Helm chart for Redis-Sentinel HA
- Extra
  - Liveness & Readiness probes
  - Resource Requests & Limits
  - Security: Pods don't have egress internet access
  - Security: SecurityContext `runAsNonRoot`, `runAsUser`, `readOnlyRootFilesystem`, `allowPrivilegeEscalation`
  - Security: [Disabled auto-mounting of ServiceAccount tokens](https://aws.github.io/aws-eks-best-practices/security/docs/iam/#disable-auto-mounting-of-service-account-tokens)

### Deployment
- [GitlabCI pipeline](.gitlab-ci.yml)
  - `validate`
    - Terraform
    - Kubectl
    - Helm
  - `build` - build and package app as a Docker image.
  - `preview` - TODO
  - `push` - push produced app image to [Gitlab container registry](https://gitlab.com/armab/click-app-gitops-22/container_registry). Push 2 tags: `git_sha1` and `latest`.
  - `deploy` - Deploy K8s objects to the remote K8s cluster (production namespace) via Helm.
    - TODO: provide latest build docker image via Helm CI_COMMIT_SHORT_SHA var
    - rollback to a previous Helm & Docker tag version in case of `helm deploy` failed

## Future Improvements
### Security
  - K8s EKS cluster security best practices
    - https://aws.github.io/aws-eks-best-practices/security/docs/
  - Different IAM users for `Terraform` `K8s` prod, `K8s` preview envs with min permissions
  - K8s RBAC for the different namespaces/users `prod` vs `preview`
  - K8s [Network Policies](https://kubernetes.io/docs/concepts/services-networking/network-policies/) - allow `app` -> `Redis` connection, drop all other traffic
  - Docker image pull/push signing/verification:
    - enable `docker trust`/signing
      - https://docs.docker.com/engine/security/trust/
      - https://dockerlabs.collabnix.com/advanced/security/trust-basics/
    - `sigstore`/`cosign`
      - https://rcarrata.com/kubernetes/sign-images-1/
### Kubernetes & Helm
  - Use external Helm chart for Redis-cluster Sentinel HA
    - https://github.com/bitnami/charts/tree/master/bitnami/redis-cluster
### CI/CD
  - Caching
  - Add e2e test via [`helm test`](https://helm.sh/docs/topics/chart_tests/) as a final check before `helm rollback`
  - Add [Helm UnitTests](https://github.com/quintush/helm-unittest)
  - Idempotence
    - Don't build and push new image in case when Docker image didn't change
    - Don't helm release same chart content every time
  - Better deployment control via Releases/Tagging `v0.1.0`
    - https://docs.gitlab.com/ee/user/project/releases/
      - On PR - create a new ephemeral preview env
      - (?) On merge to master - deploy to dev environment
      - On new git tag - release new version to prod
  - Pin Docker images to a specific SHA hash instead of `latest`

## Test and Deploy
- [ ] [Get started with GitLab CI/CD](https://docs.gitlab.com/ee/ci/quick_start/index.html)
- [ ] [Deploy to Kubernetes, Amazon EC2, or Amazon ECS using Auto Deploy](https://docs.gitlab.com/ee/topics/autodevops/requirements.html)
- [ ] [Use pull-based deployments for improved Kubernetes management](https://docs.gitlab.com/ee/user/clusters/agent/)
