stages:
  - validate
  - build
  - push
  - deploy

variables:
  K8S_PRODUCTION_NAMESPACE: production
  K8S_PREVIEW_NAMESPACE: preview
  CONTAINER_BUILD_IMAGE: $CI_REGISTRY_IMAGE/click:$CI_COMMIT_SHORT_SHA
  CONTAINER_RELEASE_IMAGE: $CI_REGISTRY_IMAGE/click:latest
  KUBE_CONTEXT: armab/click-app-gitops-22:eks

terraform-validate:
  stage: validate
  image: registry.gitlab.com/gitlab-org/terraform-images/stable:latest
  before_script:
    - cd ./terraform
  script:
    - terraform fmt -check
    - terraform init -backend=false
    - terraform validate

# Helm lint
helm-lint:
  stage: validate
  image: alpine/k8s:1.22.9
  before_script:
    - cd ./kubernetes
    - kubectl config use-context ${KUBE_CONTEXT}
  script:
    - helm lint .

# Validate the K8s manifests
k8s-validate:
  stage: validate
  image: alpine/k8s:1.22.9
  # Uses Gitlab CI/CD K8s Agent: https://docs.gitlab.com/ee/user/clusters/agent/install/index.html
  # TODO Security: Multiple agents for prod vs preview. Impersonate the agent, add RBAC: https://docs.gitlab.com/ee/user/clusters/agent/ci_cd_workflow.html#impersonate-the-cicd-job-that-accesses-the-cluster
  before_script:
    - cd ./kubernetes
    - kubectl config use-context ${KUBE_CONTEXT}
    - kubectl config set-context --current --namespace=${K8S_PRODUCTION_NAMESPACE}
  script:
    - helm template . | kubectl apply --validate=true --dry-run=client -f -

# Build docker image on every commit
# and save .tar to the CI artifacts
docker-build:
  stage: build
  image: docker:stable-git
  services:
    - docker:dind
  script:
    - docker build --tag "${CONTAINER_BUILD_IMAGE}" app/
  after_script:
    - mkdir .images
    - docker save "${CONTAINER_BUILD_IMAGE}" > .images/click:${CI_COMMIT_SHORT_SHA}.tar
  artifacts:
    paths:
      - .images/
    expire_in: 1 hour

# Retrieve docker image .tar from the artifacts
# and push to the Docker registry
# Uses Gitlab Container registry: https://docs.gitlab.com/ee/user/packages/container_registry/index.html#build-and-push-by-using-gitlab-cicd
docker-push:
  stage: push
  image: docker:stable-git
  services:
    - docker:dind
  needs:
    - job: docker-build
      artifacts: true
  before_script:
    - docker load -i .images/click:${CI_COMMIT_SHORT_SHA}.tar
    # pass registry credentials via recommended docker --password-stdin
    - echo "${CI_REGISTRY_PASSWORD}" | docker login -u "${CI_REGISTRY_USER}" --password-stdin ${CI_REGISTRY}
  script:
    - docker tag "${CONTAINER_BUILD_IMAGE}" "${CONTAINER_RELEASE_IMAGE}"
    - docker images
    - docker push "${CONTAINER_BUILD_IMAGE}"
    - docker push "${CONTAINER_RELEASE_IMAGE}"
  only:
    - main

# Apply the Helm/K8s changes in prod environment
k8s-deploy:
  stage: deploy
  image: alpine/k8s:1.22.9
  needs:
    - job: k8s-validate
    - job: helm-lint
    - job: docker-push
  before_script:
    - cd ./kubernetes
    - kubectl config use-context ${KUBE_CONTEXT}
    - kubectl config set-context --current --namespace=${K8S_PRODUCTION_NAMESPACE}
    - kubectl get all
  script:
    - helm upgrade production --set image.tag="$CI_COMMIT_SHORT_SHA" --wait --timeout 1m . || FAILED=true
    - kubectl get all
    - |
      if [ $FAILED ]; then
        echo "Upgrade failed! Rolling back Helm deployment to the previous version ..."
        helm rollback production --wait --timeout 1m
      fi
    - |
      export SERVICE_IP=$(kubectl get svc --namespace production production-demo --template "{{ range (index .status.loadBalancer.ingress 0) }}{{.}}{{ end }}")
      echo "App URL: http://$SERVICE_IP:80/"
  only:
    - main

# TODO: $CI_MERGE_REQUEST_IID for preview

# TODO: Delete image for the preview ephemeral env
# https://docs.gitlab.com/ee/user/packages/container_registry/index.html#delete-images-using-gitlab-cicd
# docker-delete:
#   stage: asd
