# Python3 Flask app

The demo app tracks number of clicks and uses Redis as a storage.
It's built and distributed as a Docker image and is a web service listening on port `80`.

## Usage
Build the image:
```shell
docker build -t registry.gitlab.com/armab/click-app-gitops-22/click:latest . 
```

## ENV variables
- `REDIS_URL` - Redis endpoint to connect
