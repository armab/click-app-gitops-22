variable "app_name" {
  default     = "gitops-demo"
  description = "Name of the app, used in templating everywhere"
  type        = string
}

variable "aws_region" {
  description = "AWS region to launch servers in"
  default     = "eu-west-1"
}

variable "vpc_cidr" {
  default     = "10.0.0.0/16"
  description = "10.0.0.0/16 = 65K IPs. 10.0.0.0 - 10.0.255.255 range"
  type        = string
}

variable "vpc_public_subnets" {
  default     = ["10.0.101.0/24", "10.0.102.0/24"]
  description = "Subnets that may have public IP and inet ingress. 10.0.101.0/24 = 256 IPs. 10.0.101.0 - 10.0.101.255 range"
  type        = list(string)
}

variable "vpc_private_subnets" {
  default     = ["10.0.1.0/24", "10.0.2.0/24"]
  description = "Subnets that are 10.0.1.0/24 = 256 IPs. 10.0.1.0 - 10.0.1.255 range"
  type        = list(string)
}
