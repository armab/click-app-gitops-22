# Create AWS EKS cluster
# https://registry.terraform.io/modules/terraform-aws-modules/eks/aws/latest
module "eks" {
  source  = "terraform-aws-modules/eks/aws"
  version = "18.23.0"

  cluster_name    = var.app_name
  cluster_version = "1.22"

  # Rely on previously created VPC and Private Subnets
  vpc_id                         = module.vpc.vpc_id
  subnet_ids                     = module.vpc.private_subnets
  cluster_endpoint_public_access = true

  # TODO prod: Scale up for the real environment. Lowcost config now.
  eks_managed_node_groups = {
    default = {
      instance_types = ["t2.small"]
      desired_size   = 2
      min_size       = 2
      max_size       = 5
    }
  }

  # TODO: AMI/groups/permissions
  # TODO: manage aws-auth configmap (https://github.com/terraform-aws-modules/terraform-aws-eks#usage)
  # TODO: K8s RBAC for different users
  # TODO: Cluster encryption, KMS key config
  # TODO: Cluster addons, CNI

  tags = {
    Application = var.app_name
    Tier        = "k8s"
    Environment = "dev"
    Terraform   = "true"
  }
}
