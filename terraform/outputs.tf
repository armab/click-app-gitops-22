output "vpc_id" {
  description = "The ID of the VPC"
  value       = module.vpc.vpc_id
}

##################
# K8s Outputs
output "eks_id" {
  description = "EKS cluster name"
  value       = module.eks.cluster_id
}

output "eks_endpoint" {
  description = "EKS cluster endpoint URI"
  value       = module.eks.cluster_endpoint
}

output "k8s_cli" {
  description = "Configure kubeconfig as per AWS EKS doc: https://docs.aws.amazon.com/eks/latest/userguide/create-kubeconfig.html"
  value       = "aws eks update-kubeconfig --region ${var.aws_region} --name ${module.eks.cluster_id} --alias eks"
}
