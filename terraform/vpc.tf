provider "aws" {
  region = var.aws_region
}

# Retrieve list of AZs
data "aws_availability_zones" "available" {}

# Create a dedicated VPC for K8s cluster
# https://registry.terraform.io/modules/terraform-aws-modules/vpc/aws/3.14.0
module "vpc" {
  source  = "terraform-aws-modules/vpc/aws"
  version = "3.14.0"

  name               = var.app_name
  cidr               = var.vpc_cidr
  azs                = data.aws_availability_zones.available.names
  private_subnets    = var.vpc_private_subnets
  public_subnets     = var.vpc_public_subnets
  enable_nat_gateway = true
  # TODO prod: Multiple NAT gateways for every AZ
  single_nat_gateway   = true
  enable_dns_hostnames = true

  tags = {
    Application = var.app_name
    Terraform   = "true"
    Environment = "dev"
  }

  vpc_tags = {
    Name = "${var.app_name}-vpc"
  }

  # Subnet auto-discovery by EKS load-balancers and ingress controllers
  # https://aws.amazon.com/premiumsupport/knowledge-center/eks-vpc-subnet-discovery/
  public_subnet_tags = {
    "kubernetes.io/cluster/${var.app_name}" = "shared"
    "kubernetes.io/role/elb"                = "1"
  }
  private_subnet_tags = {
    "kubernetes.io/cluster/${var.app_name}" = "shared"
    "kubernetes.io/role/internal-elb"       = "1"
  }
}
