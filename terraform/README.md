# Terraform for VPC and K8s cluster

Create AWS VPC and AWS EKS K8s cluster

See `variables.tf` for configuration.

### AWS VPC
  * 2 public, 2 private subnets
  * 2 Availability Zones
  * tagging
### AWS EKS
  * 2 nodes by default
  * Autoscaling for nodes

## Usage
```shell
terraform plan
terraform apply

# Helper for k8s cli
terraform output k8s_cli
```

# TODO
* Amazon EKS Security Best Practices
  * https://aws.github.io/aws-eks-best-practices/security/docs/
