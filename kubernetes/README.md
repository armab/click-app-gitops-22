# K8s Helm charts for the app

See Helm [values](./values.yaml) for full configuration.

## Usage
```
# install chart into new namespace
helm install production -n production --create-namespace --wait --timeout 1m .

# upgrade on changes
helm upgrade production -n production --wait --timeout 1m .

# show release history
helm history -n production production

# rollback the release in case of failures
helm rollback production -n production --wait --timeout 1m

# uninstall the deployment
helm uninstall production -n production
```
